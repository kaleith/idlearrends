﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollow : MonoBehaviour
{
    public GameObject ToFollow;
    public float followSmoothing =1;
    public Vector3 offset , smoothing;
    public Vector3 originalPos,currentPos;
    public bool autoSetOffset = true;

   // float ShakeElapsedTime;
    //Vector3 shakeFactor;

   
    void Awake()
    {
        currentPos = transform.position;
        originalPos = transform.position;
        if (ToFollow != null&& autoSetOffset)
        {
            setOffset(ToFollow.transform);
        }
    }

    
    public void setOffset(Transform follow)
    {
        offset = originalPos - follow.transform.position;
        currentPos = transform.position;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if (ToFollow != null)
        {
            currentPos = Vector3.SmoothDamp(currentPos, ToFollow.transform.position + offset, ref smoothing, followSmoothing);
            transform.position = currentPos ;
        }

    }
    /*

    public void ShakeIt(float dur, float mag)
    {
        if(ShakeElapsedTime==0)
          StartCoroutine(shakeit(dur, mag));


    }

    IEnumerator shakeit(float duration, float magnitude)
    {

        ShakeElapsedTime = 0;

        while (ShakeElapsedTime < duration)
        {
            shakeFactor = Vector3.zero;

            ShakeElapsedTime += Time.deltaTime;


            float x = Random.Range(-1f, 1f) * magnitude * Time.timeScale;
            float y = Random.Range(-1f, 1f) * magnitude * Time.timeScale;
            // float z = Random.Range(-1f, 1f) * magnitude;


            shakeFactor.x = x;
            shakeFactor.y = y;

            yield return null;
        }

        shakeFactor = Vector3.zero;
        duration = 0;
        ShakeElapsedTime = 0;

    }
    */

}
