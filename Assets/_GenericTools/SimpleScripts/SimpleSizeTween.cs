﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSizeTween : MonoBehaviour
{
    [SerializeField]AnimationCurve curve;
    Vector3 originalScale;
    bool IsStarted;
    [SerializeField] float speed;
    public float duration;
    public delegate void CallBack(GameObject gObject);
    CallBack callBack;
    float startTime;

    void Start()
    {
        originalScale = transform.localScale;
    }


    public void StartAnimation(float speed, float duration, AnimationCurve curve, CallBack callBack)
    {
        this.callBack = callBack;

        StartAnimation(speed, duration, curve);
    }

    public void StartAnimation(float speed, float duration,AnimationCurve curve)
    {   if (curve != null)
        this.curve = curve;
        StartAnimation(speed, duration);
    }

    public void StartAnimation(float speed,float duration)
    {
        this.speed = speed;
        this.duration = duration;
        startTime = Time.time;
        IsStarted = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (IsStarted)
        {
            transform.localScale = curve.Evaluate((Mathf.Clamp(Time.time- startTime, 0,duration) * speed) % 1) * originalScale;

            if (startTime + duration < Time.time)
            {
                IsStarted = false;
                transform.localScale = originalScale;
                if (callBack != null)
                    callBack.Invoke(gameObject);

            }


        }
        
    }
}
