﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Simple3rdNavMeshPersonMovement : MonoBehaviour
{
    public float speed { 
    get{
            return GameManager.parameters.speed;

        }
    }


    public float targetAngle,currentAngle;
    public Joystick stick;
    public bool ControllEnabled = true;
    public float turnSpeed = 3;
    Animator animator;
    NavMeshAgent agent;
    Vector3 directionVector;
    bool oldRun = false, run;
    Quaternion targetRotation;
    public static bool inputExists;

    private void LateUpdate()
    {
        if (ControllEnabled)
        {
            directionVector.x = stick.Horizontal;
            directionVector.z = stick.Vertical;
            run = directionVector.magnitude > 0;

            if (oldRun != run)
            {
                if(animator!=null)
                animator.SetBool("run", run);
                oldRun = run;
            }

            if (directionVector.magnitude == 0)
            {
                inputExists = false;
                return;
            }
            else
                inputExists = true;

            agent.Move(directionVector * Time.deltaTime * speed);
            targetAngle = Mathf.Atan2(directionVector.x, directionVector.z) * Mathf.Rad2Deg;
            targetRotation = Quaternion.Euler(0, targetAngle, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * turnSpeed);
        }

    }

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        agent = GetComponent<NavMeshAgent>();
        targetRotation = transform.rotation;
    }


}
