﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Docker : MonoBehaviour
{

    public float speed
    {
        get
        {
            return GameManager.parameters.speed;

        }
    }

    public bool playerInputExists
    {
        get {
            return Simple3rdNavMeshPersonMovement.inputExists;
        }
    }
    public  bool isPlayerInDockRange{
        get {
            return Vector3.Distance(transform.position, dockingPosition) < GameManager.parameters.DockingRange;
        }
    }
    public Vector3 dockingPosition {
        get{
            return DockingStation.dockingBody.transform.position;
        }
    }
    public Quaternion DockingRotation
    {
        get
        {
            //CALCULATE THE RIGHT DOCKING ANGLE
            return DockingStation.dockingBody.transform.rotation;
        }
    }


    public bool isBatteryFull {
        get{
            return GameManager.parameters.currentBattery == GameManager._instance._parameters.batteryMax;
        }
    }

    public bool isBinEmpty
    {
        get
        {
            return GameManager.parameters.currentBin == 0;
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (!playerInputExists && isPlayerInDockRange && (!isBatteryFull||!isBinEmpty))
        {
            transform.position = Vector3.MoveTowards(transform.position, dockingPosition, speed * Time.deltaTime);
           //ROTATE TO RIGTH DOCKING ANGLE;
        }
        
    }
}
