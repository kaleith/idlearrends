﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public static Player _instance;
    public GameObject PlayerModel;
    public Animator animator;


    private void Awake()
    {
        _instance = this; 
    }

    private void Start()
    {
        loadPlayerModel();
    }
    private void loadPlayerModel()
    {
        PlayerModel = Instantiate(GameManager.PlayerCharacter.model,transform);
        animator = PlayerModel.GetComponent<Animator>();
    }


}
