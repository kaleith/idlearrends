﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryUpdater : MonoBehaviour
{
    public float DepleetSpeed;
    Vector3 oldPosition;
    void Start()
    {
        oldPosition = transform.position;
    }

    void UpdateBattery()
    {
        float distance = Vector3.Distance(oldPosition, transform.position);
        GameManager.parameters.currentBattery -= distance * DepleetSpeed;
        oldPosition = transform.position;
    }
    private void LateUpdate()
    {
        if (GameManager.parameters.state == GameState.play)
        {
            UpdateBattery();
        }
    }
}
