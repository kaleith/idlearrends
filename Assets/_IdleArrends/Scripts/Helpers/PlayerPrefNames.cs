﻿namespace idleArrends
{
    public class PlayerPrefNames 
    {
        private PlayerPrefNames(string value) { Value = value; }
        public string Value { get; set; }
        public static string playerName   { get { return "playerName"; } }
        public static string level   { get { return "level"; } }
        public static string coins { get { return "coins"; } }
    }    
}