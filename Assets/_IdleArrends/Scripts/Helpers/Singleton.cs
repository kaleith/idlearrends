using UnityEngine;
/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T _instance;
    private static bool _applicationIsQuitting;
    public static T Instance
    {
        get
        {
            if (!_instance && !_applicationIsQuitting)
            {
                _instance = FindObjectOfType<T>();
                if (_instance)
                    Debug.Log(string.Format("[Singleton] Assigned instance for {0}.", _instance.GetType().FullName));
            }
            return _instance;
        }
    }
    protected virtual void OnAwake()
    {
    }
    protected virtual void BeforeOnDestroy()
    {
    }
    protected void Awake()
    {
        if (!_instance)
        {
            _instance = (T) this;
            Debug.Log(string.Format("[Singleton] Assigned instance for {0}.", _instance.GetType().FullName));
        }
        OnAwake();
    }
    protected void OnDestroy () 
    {
        BeforeOnDestroy();
        if (_instance == this)
        {
            Debug.Log(string.Format("[Singleton] Cleared instance for {0}.", _instance.GetType().FullName));
            _instance = null;
        }
    }
    protected void OnApplicationQuit()
    {
        _applicationIsQuitting = true;
    }
}