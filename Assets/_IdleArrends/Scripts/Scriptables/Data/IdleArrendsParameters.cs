﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName ="parameters", menuName ="Scriptables/Data/InGameParameters")]
public class IdleArrendsParameters : InGameParameters
{
    public  float DEFAULT_BATTERY_MAX = 100f;
    public  float DEFAULT_CHARGE_PER_SEC = 10f;
    public  float DEFAULT_BIN_CAPACITY = 100f;
    public  float DEFAULT_BIN_CLEAN_PER_SEC = 10f;

    string PLAYERPREF_NAME_BATTERY_MAX = "PP_batery_max";
    string PLAYERPREF_NAME_CHARGE_PER_SEC = "PP_charge_ps";
    string PLAYERPREF_NAME_BIN_CAPACITY = "PP_bin_capacity";
    string PLAYERPREF_NAME_BIN_CLEAN_PER_SEC = "PP_bin_clean_ps";

    public float DockingRange =1;
    public float speed;

    public float batteryMax
    {
        get
        {
            if(_batteryMax == 0)
                _batteryMax = PlayerPrefs.GetFloat(PLAYERPREF_NAME_BATTERY_MAX, DEFAULT_BATTERY_MAX);
            return _batteryMax;
        }
        set
        {
            _batteryMax = value;
            PlayerPrefs.GetFloat(PLAYERPREF_NAME_BATTERY_MAX, value);
            
        }
    }
    static float _batteryMax;
    public float chargeUnits
    {
        get
        {
            if(_chargeUnits==0)
                _chargeUnits =  PlayerPrefs.GetFloat(PLAYERPREF_NAME_CHARGE_PER_SEC, DEFAULT_CHARGE_PER_SEC);
            return _chargeUnits;

        }
        set
        {
            _chargeUnits = value;
            PlayerPrefs.GetFloat(PLAYERPREF_NAME_CHARGE_PER_SEC, value);

        }
    }
    static float _chargeUnits;

    public float binCapacity
    {
        get
        {

            if(_binCapacity==0)
                _binCapacity = PlayerPrefs.GetFloat(PLAYERPREF_NAME_BIN_CAPACITY, DEFAULT_BIN_CAPACITY); 
            return _binCapacity;
        }
        set
        {
            _binCapacity = value;
            PlayerPrefs.GetFloat(PLAYERPREF_NAME_BIN_CAPACITY, value);

        }
    }
    static float _binCapacity;

    public float cleanUnits
    {
        get
        {
            if(_cleanUnits==0)
                _cleanUnits = PlayerPrefs.GetFloat(PLAYERPREF_NAME_BIN_CLEAN_PER_SEC, DEFAULT_BIN_CLEAN_PER_SEC);
            return _cleanUnits;
        }
        set
        {
            _cleanUnits = value;
            PlayerPrefs.GetFloat(PLAYERPREF_NAME_BIN_CLEAN_PER_SEC, value);

        }
    }
    static float _cleanUnits;

    public float currentBatteryPercentage
    {
        get
        {
            return currentBattery / batteryMax;

        }
    }

    public float currentBattery {
        get {
            return _currentBattery;
        }
        set {

            if (value != _currentBattery)
            {
                _currentBattery = Mathf.Clamp(value,0,batteryMax);
                if(OnBatteryChange!=null)
                    OnBatteryChange.Invoke();

            }

            if (_currentBattery == 0 && OnBatteryEmpty!=null)
                OnBatteryEmpty.Invoke();
        }
    }
    float _currentBattery;

    public float currentBin
    {
        get
        {
            return _currentBin;
        }
        set
        {

            if (value != _currentBin)
            {
                _currentBin = Mathf.Clamp(value, 0, binCapacity);
                if(OnBinChange!=null)
                    OnBinChange.Invoke();

            }

            if (_currentBattery == binCapacity&& OnBinFull!=null)
                OnBinFull.Invoke();
        }
    }
    float _currentBin;
 
    public override void resetParameters()
    {
        base.resetParameters();
        _currentBattery = batteryMax;
        _currentBin = 0;
    }
    public override void ResetToDefaults()
    {
        base.ResetToDefaults();
        batteryMax = DEFAULT_BATTERY_MAX;
        binCapacity = DEFAULT_BIN_CAPACITY;
        chargeUnits = DEFAULT_CHARGE_PER_SEC;
        cleanUnits = DEFAULT_BIN_CLEAN_PER_SEC;

    }

    public event Action OnBatteryEmpty;
    public event Action OnBinFull;
    public event Action OnBatteryChange;
    public event Action OnBinChange;



}
