﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public IdleArrendsGameSettings _settings;
    public IdleArrendsParameters _parameters;

    public static IdleArrendsGameSettings settings
    {
        get{
            return _instance._settings;
        
        }
    }

    public static IdleArrendsParameters parameters
    {
        get
        {
            return _instance._parameters;

        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public static RoombaCharacterData PlayerCharacter
    {
        get {
            return _instance._settings.AvailableCharacters.scriptableObjects[_instance._parameters.selectedCharacter] as RoombaCharacterData;
        }
    }

    private void Start()
    {
        parameters.resetParameters();
        parameters.state = GameState.play;
    }
}
