﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBatteryCapacity : MonoBehaviour
{
    public Gradient ColorScheme;
    Image capacityImage;
    void Start()
    {
        capacityImage = GetComponent<Image>();
        GameManager.parameters.OnBatteryChange += capacityUpdated;
        capacityUpdated();
    }

    public void capacityUpdated()
    {
       
        capacityImage.fillAmount = GameManager.parameters.currentBatteryPercentage;
        capacityImage.color = ColorScheme.Evaluate(capacityImage.fillAmount);
    }

    private void OnDestroy()
    {
        GameManager.parameters.OnBatteryChange -= capacityUpdated;
    }

}
